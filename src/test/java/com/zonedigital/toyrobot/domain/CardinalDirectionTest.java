package com.zonedigital.toyrobot.domain;

import static com.zonedigital.toyrobot.domain.CardinalDirection.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.zonedigital.toyrobot.domain.CardinalDirection;
import com.zonedigital.toyrobot.domain.PlaneCoordinate;

public class CardinalDirectionTest {

	
	
	@Nested
	public class Rotate {
		
		
		@Nested
		public class WhenFacing {

			@Nested
			public class North {
				
				private CardinalDirection north = NORTH; 

				@Test
				void thenTurningLeftReturnsWest()
				{
					assertThat(north.left(), is(WEST));
				}

				@Test
				void thenTurningRightReturnsEast()
				{
					assertThat(north.right(), is(EAST));
				}
			}


			@Nested
			public class East {
				
				private CardinalDirection east = EAST; 

				@Test
				void thenTurningLeftReturnsNorth()
				{
					assertThat(east.left(), is(NORTH));
				}

				@Test
				void thenTurningRightReturnsSouth()
				{
					assertThat(east.right(), is(SOUTH));
				}
			}


			@Nested
			public class South {
				
				private CardinalDirection south = SOUTH; 

				@Test
				void thenTurningLeftReturnsEast()
				{
					assertThat(south.left(), is(EAST));
				}

				@Test
				void thenTurningRightReturnsWest()
				{
					assertThat(south.right(), is(WEST));
				}
			}

			@Nested
			public class West {
				
				private CardinalDirection west = WEST; 

				@Test
				void thenTurningLeftReturnsSouth()
				{
					assertThat(west.left(), is(SOUTH));
				}

				@Test
				void thenTurningRightReturnsNorth()
				{
					assertThat(west.right(), is(NORTH));
				}
			}
			
		}
	}	
	
	
	@Nested
	public class Increment {
		
		@Nested
		public class GivenOrigin {

			private final PlaneCoordinate origin = new PlaneCoordinate(0, 0);

			@Nested
			public class IncrementBySingleUnitWhenFacing {

				@Test
				void north()
				{
					PlaneCoordinate incremented = NORTH.increment(origin);

					assertThat(incremented, is(new PlaneCoordinate(0, 1)));

				}


				@Test
				void south()
				{
					PlaneCoordinate incremented = SOUTH.increment(origin);

					assertThat(incremented, is(new PlaneCoordinate(0, -1)));

				}


				@Test
				void east()
				{
					PlaneCoordinate incremented = EAST.increment(origin);

					assertThat(incremented, is(new PlaneCoordinate(1, 0)));

				}


				@Test
				void west()
				{
					PlaneCoordinate incremented = WEST.increment(origin);

					assertThat(incremented, is(new PlaneCoordinate(-1, 0)));

				}
			}

			@Nested
			public class IncrementByMultipleUnitsWhenFacing {

				@Test
				void north()
				{
					PlaneCoordinate incremented = NORTH.increment(origin, 42);

					assertThat(incremented, is(new PlaneCoordinate(0, 42)));

				}
			}
		}

		@Nested
		public class GivenArbitraryStartingCoordinate {

			private final PlaneCoordinate start = new PlaneCoordinate(-101, 42);

			@Nested
			public class IncrementBySingleUnitWhenFacing {

				@Test
				void north()
				{
					PlaneCoordinate incremented = NORTH.increment(start);

					assertThat(incremented, is(new PlaneCoordinate(-101, 43)));

				}

				@Test
				void west()
				{
					PlaneCoordinate incremented = WEST.increment(start);

					assertThat(incremented, is(new PlaneCoordinate(-102, 42)));

				}
			}
		}
	}
}
