package com.zonedigital.toyrobot.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.zonedigital.toyrobot.domain.PlaneCoordinate;

public class PlaneCoordinateTest {
	
	@Nested
	public class Equality {

		private final int x = 1;
		private final int y = 2;
		private final PlaneCoordinate a = new PlaneCoordinate(x, y);

		@DisplayName("Are equal")
		@Nested
		public class AreEqual {


			@DisplayName("when X and Y are identical")
			@Test
			public void whenXAndYAreIdentical()
			{
				PlaneCoordinate b = new PlaneCoordinate(x, y);
				
				assertThat(a, is(equalTo(b)));
				assertThat(a.hashCode(), is(equalTo(b.hashCode())));
			}

			@DisplayName("when B is cast as Object")
			@Test
			public void whenBIsCastToObject()
			{
				Object b = new PlaneCoordinate(x, y);
				
				assertTrue(a.equals(b));
			}
		}

		
		@DisplayName("Are not equal")
		@Nested
		public class AreNotEqual {

			@DisplayName("when X values differ")
			@Test
			void whenXValuesDiffer() throws Exception
			{
				PlaneCoordinate b = new PlaneCoordinate(-x, y);
				
				assertThat(a, is(not(equalTo(b))));
			}

			@DisplayName("when Y values differ")
			@Test
			void whenYValuesDiffer() throws Exception
			{
				PlaneCoordinate b = new PlaneCoordinate(x, -y);
				
				assertThat(a, is(not(equalTo(b))));
			}

			@DisplayName("when both X and Y values differ")
			@Test
			void whenXAndYValuesDiffer() throws Exception
			{
				PlaneCoordinate b = new PlaneCoordinate(-x, -y);
				
				assertThat(a, is(not(equalTo(b))));
			}

			@DisplayName("when B is null type")
			@Test
			void whenBIsNullType() throws Exception
			{
				PlaneCoordinate b = null;
				
				assertFalse(a.equals(b));
			}

			@DisplayName("when B is null object")
			@Test
			void whenBIsNullObject() throws Exception
			{
				Object b = null;
				
				assertFalse(a.equals(b));
			}
		}
	}


	
	@Nested
	public class Comparable {


		@Test
		public void lessThanWhenXIsSmaller()
		{
			PlaneCoordinate a = new PlaneCoordinate(11, 9);
			PlaneCoordinate b = new PlaneCoordinate(12, 5);
			
			assertThat(a.lessThan(b), is(true));
		}


		@Test
		public void lessThanWhenYIsSmaller()
		{
			PlaneCoordinate a = new PlaneCoordinate(12, 4);
			PlaneCoordinate b = new PlaneCoordinate(12, 5);
			
			assertThat(a.lessThan(b), is(true));
		}


		@Test
		public void notLessThanWhenYIsLarger()
		{
			PlaneCoordinate a = new PlaneCoordinate(12, 42);
			PlaneCoordinate b = new PlaneCoordinate(12,  5);
			
			assertThat(a.lessThan(b), is(false));
		}


		@Test
		public void greaterThanWhenXIsLarger()
		{
			PlaneCoordinate a = new PlaneCoordinate(42, 1);
			PlaneCoordinate b = new PlaneCoordinate(21, 9);
			
			assertThat(a.greaterThanOrEqualTo(b), is(true));
		}


		@Test
		public void notGreaterThanWhenXIsSmaller()
		{
			PlaneCoordinate a = new PlaneCoordinate(21, 9);
			PlaneCoordinate b = new PlaneCoordinate(42, 1);
			
			assertThat(a.greaterThanOrEqualTo(b), is(false));
		}


		@Test
		public void greaterThanWhenYIsLarger()
		{
			PlaneCoordinate a = new PlaneCoordinate(101, 9);
			PlaneCoordinate b = new PlaneCoordinate(101, 1);
			
			assertThat(a.greaterThanOrEqualTo(b), is(true));
		}


		@Test
		public void equalTo()
		{
			PlaneCoordinate a = new PlaneCoordinate(21, 42);
			PlaneCoordinate b = new PlaneCoordinate(21, 42);
			
			assertThat(a.greaterThanOrEqualTo(b), is(true));
		}
		
	}

	
	@Nested
	public class Properties {
		
		private final PlaneCoordinate coordinate = new PlaneCoordinate(10, 20);
		
		
		@DisplayName("1st constructor arg is X")
		@Test
		void firstConstructorArgumentIsX()
		{
			assertThat(coordinate.x(), is(10));
		}

		@DisplayName("2nd constructor arg is X")
		@Test
		void secondConstructorArgumentIsY()
		{
			assertThat(coordinate.y(), is(20));
		}

		@DisplayName("Coordinates are ordered X then Y")
		@Test
		void xAndYReturnedInOrder()
		{
			assertThat(coordinate.coordinate(), is(equalTo(new int[] {10, 20})));
		}

		@DisplayName("Has 2 dimensions by definition")
		@Test
		void hasTwoDimensionsByDefinition()
		{
			assertThat(coordinate.dimensions(), is(2));
		}
	}
}
