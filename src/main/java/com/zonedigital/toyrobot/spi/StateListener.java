package com.zonedigital.toyrobot.spi;

import com.zonedigital.toyrobot.domain.CardinalState;

/**
 * State update is ordered but updates not guaranteed to be by same thread.
 */
//@javax.annotation.concurrent.ThreadSafe
public interface StateListener {

	void update(CardinalState state);
}
