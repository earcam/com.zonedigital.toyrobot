package com.zonedigital.toyrobot.domain;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.Test;

import com.zonedigital.toyrobot.domain.Coordinate;

public class CoordinateTest {

	@SuppressWarnings("serial")
	private static class DummyCoordinate implements Coordinate<DummyCoordinate> {

		private int[] coordinate;


		public DummyCoordinate(int[] coordinate)
		{
			this.coordinate = coordinate;
		}


		@Override
		public int[] coordinate()
		{
			return coordinate;
		}
	}


	@Test
	void dimensionsDefaultsToInstancesCoordinateLength()
	{
		DummyCoordinate coordinate = new DummyCoordinate(new int[5]);

		assertThat(coordinate.dimensions(), is(5));
	}


	@Test
	void shorterCoordinateIsAlwaysLessThan()
	{
		DummyCoordinate longer = new DummyCoordinate(new int[] { 1, 1, 1, 1 });
		DummyCoordinate shorter = new DummyCoordinate(new int[] { 1_000 });

		assertThat(longer, is(greaterThan(shorter)));
	}
}
