package com.zonedigital.toyrobot.app.cli;

import com.zonedigital.toyrobot.spi.Command;

class CommandAction implements Action {
	final Command command;


	CommandAction(Command command)
	{
		this.command = command;
	}


	public Command command()
	{
		return command;
	}
}
