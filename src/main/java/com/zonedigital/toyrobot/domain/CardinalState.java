package com.zonedigital.toyrobot.domain;

public class CardinalState extends State<PlaneCoordinate, CardinalDirection, CardinalState> {

	private static final long serialVersionUID = -4532244000522560640L;

	public CardinalState(PlaneCoordinate coordinate, CardinalDirection direction)
	{
		super(coordinate, direction);
	}

	@Override
	protected CardinalState create(PlaneCoordinate coordinate, CardinalDirection direction)
	{
		return new CardinalState(coordinate, direction);
	}

}
