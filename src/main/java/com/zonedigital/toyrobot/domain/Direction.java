package com.zonedigital.toyrobot.domain;

import java.io.Serializable;

/**
 * A {@link Direction} is responsible for incrementing a Coordinate.
 * 
 * In this exercise only {@link PlaneCoordinate} is considered.
 * 
 * So given a <i>move</i> is defined as a number of units in a <i>direction</i>,
 * and that <i>direction</i> is dependent on a <i>coordinate system</i>.
 * We could swap in another enumeration for e.g. inter-cardinal directions then
 * SW/WSW may be considered.
 *
 * @param <D> the specific Direction enumeration
 * @param <C> the Coordinate (system) this Direction can handle
 */
public interface Direction<C extends Coordinate<C>, D extends Enum<D> & Direction<C, D>> extends Serializable {

	public default C increment(C coordinate)
	{
		return increment(coordinate, 1);
	}

	public abstract C increment(C coordinate, int units);
	
	/**
	 * @return return the element following this - anti-clockwise direction 
	 */
	public abstract D left();
	
	/**
	 * @return return the element following this - clockwise direction 
	 */
	public abstract D right();
}
