package com.zonedigital.toyrobot.spi;

public interface Engine {

	public abstract void register(StateListener stateListener);

	public abstract void execute(Command command);
}
