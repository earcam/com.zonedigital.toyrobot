package com.zonedigital.toyrobot.domain;

import java.util.Objects;


//@javax.annotation.concurrent.Immutable
public final class PlaneCoordinate implements Coordinate<PlaneCoordinate> {

	private static final long serialVersionUID = -3658276344057690421L;

	private final int x;
	private final int y;


	public PlaneCoordinate(int x, int y)
	{
		this.x = x;
		this.y = y;
	}


	@Override
	public boolean equals(Object other)
	{
		return other instanceof PlaneCoordinate && equals((PlaneCoordinate) other);
	}


	public boolean equals(PlaneCoordinate that)
	{
		return that != null
				&& that.x() == this.x
				&& that.y() == this.y;
	}


	@Override
	public int hashCode()
	{
		return Objects.hash(x, y);
	}


	public int x()
	{
		return x;
	}


	public int y()
	{
		return y;
	}


	@Override
	public int dimensions()
	{
		return 2;
	}


	@Override
	public int[] coordinate()
	{
		return new int[] { x, y };
	}

}
