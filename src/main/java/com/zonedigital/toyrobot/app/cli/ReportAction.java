package com.zonedigital.toyrobot.app.cli;

/**
 * "REPORT" is a <i>command</i> in the UI sense, but not in the domain sense if following CQRS style.
 *
 */
class ReportAction implements Action {
	public static final ReportAction REPORT_ACTION = new ReportAction();


	private ReportAction()
	{}
}
