package com.zonedigital.toyrobot.domain;

/**
 * <a href="https://en.wikipedia.org/wiki/Cardinal_direction">wikipedia</a>
 *
 */
public enum CardinalDirection implements Direction<PlaneCoordinate, CardinalDirection> {

	NORTH(0, 1),
	EAST(1, 0),
	SOUTH(0, -1),
	WEST(-1, 0);

	// values() creates a new array for every call
	private static final CardinalDirection[] VALUES = values();

	private int deltaX;
	private int deltaY;
	
	CardinalDirection(int deltaX, int deltaY)
	{
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}


	@Override
	public PlaneCoordinate increment(PlaneCoordinate coordinate, int units)
	{
		return new PlaneCoordinate(coordinate.x() + deltaX * units, coordinate.y() + deltaY * units);
	}


	@Override
	public CardinalDirection left()
	{
		int next = ordinal() - 1;
		return (next < 0) ? VALUES[VALUES.length - 1] : VALUES[next];
	}


	@Override
	public CardinalDirection right()
	{
		int next = ordinal() + 1;
		return (next == VALUES.length) ? VALUES[0] : VALUES[next];
	}
}
