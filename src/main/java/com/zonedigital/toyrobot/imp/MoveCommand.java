package com.zonedigital.toyrobot.imp;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.spi.Command;

public class MoveCommand implements Command {

	public static final MoveCommand MOVE_COMMAND = new MoveCommand();
	
	@Override
	public CardinalState execute(CardinalState state)
	{
		return state.move();
	}

}
