package com.zonedigital.toyrobot.domain;

import static com.zonedigital.toyrobot.domain.CardinalDirection.NORTH;
import static com.zonedigital.toyrobot.domain.CardinalDirection.SOUTH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class StateTest {

	@SuppressWarnings("serial")
	private static class DummyState extends State<PlaneCoordinate, CardinalDirection, DummyState> {

		public DummyState(PlaneCoordinate coordinate, CardinalDirection direction)
		{
			super(coordinate, direction);
		}


		@Override
		protected DummyState create(PlaneCoordinate coordinate, CardinalDirection direction)
		{
			return new DummyState(coordinate, direction);
		}

	}
	
	
	@Nested
	public class Equality {
		
		
		@Test
		public void equal()
		{
			DummyState a = new DummyState(new PlaneCoordinate(0, 0), NORTH);
			DummyState b = new DummyState(new PlaneCoordinate(0, 0), NORTH);
			
			assertThat(a, is(equalTo(b)));
			assertThat(a.hashCode(), is(equalTo(b.hashCode())));
		}

		
		@Nested 
		public class NotEqual {
			
			
			@Test
			void whenCoordinatesDiffer()
			{
				DummyState a = new DummyState(new PlaneCoordinate(0, 0), NORTH);
				DummyState b = new DummyState(new PlaneCoordinate(1, 1), NORTH);
				
				assertThat(a, is(not(equalTo(b))));
			}


			@Test
			void whenDirectionsDiffer()
			{
				DummyState a = new DummyState(new PlaneCoordinate(0, 0), NORTH);
				DummyState b = new DummyState(new PlaneCoordinate(0, 0), SOUTH);
				
				assertThat(a, is(not(equalTo(b))));
			}


			@Test
			void toNullType()
			{
				DummyState a = new DummyState(new PlaneCoordinate(0, 0), NORTH);
				DummyState b = null;
				
				assertFalse(a.equals(b));
			}


			@Test
			void toNullObject()
			{
				DummyState a = new DummyState(new PlaneCoordinate(0, 0), NORTH);
				Object b = null;
				
				assertFalse(a.equals(b));
			}
		}
	}
}
