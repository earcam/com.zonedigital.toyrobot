package com.zonedigital.toyrobot.spi;

import com.zonedigital.toyrobot.domain.CardinalState;

//@javax.annotation.ParametersAreNonnullByDefault
public interface Command {

	public abstract CardinalState execute(CardinalState state);

}
