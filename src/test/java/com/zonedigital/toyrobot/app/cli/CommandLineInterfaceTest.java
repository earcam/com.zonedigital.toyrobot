package com.zonedigital.toyrobot.app.cli;

import static com.zonedigital.toyrobot.domain.CardinalDirection.EAST;
import static com.zonedigital.toyrobot.domain.CardinalDirection.NORTH;
import static com.zonedigital.toyrobot.domain.CardinalDirection.SOUTH;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.domain.PlaneCoordinate;
import com.zonedigital.toyrobot.imp.DefaultEngine;
import com.zonedigital.toyrobot.spi.Engine;

public class CommandLineInterfaceTest {

	final OutputStream noopOut = new OutputStream() {
		@Override
		public void write(int b) throws IOException
		{
		}
	};
	
	@Test
	public void asPerBrief()
	{
		String input = "PLACE 0,0,NORTH\n" +
				"MOVE\n" +
				"REPORT\n" +
				"PLACE 0,0,NORTH\n" +
				"LEFT\n" +
				"REPORT\n" +
				"PLACE 1,2,EAST\n" +
				"MOVE\n" +
				"MOVE\n" +
				"LEFT\n" +
				"MOVE\n" +
				"REPORT\n";

		String expected = "0,1,NORTH\n" +
				"0,0,WEST\n" +
				"3,3,NORTH";

		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes(UTF_8));
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(output);

		Engine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 5);
		CommandLineInterface cli = new CommandLineInterface(engine, in, out, (t,e) -> {});

		cli.run();

		String actual = new String(output.toByteArray(), UTF_8);
		assertThat(actual, is(equalToIgnoringWhiteSpace(expected)));
	}

	
	@Test
	public void failedCommandIsHandled()
	{
		String input = "PLACE 4,3,NORTH\n" +
				"RIGHT\n" +
				"RIGHT\n" +
				"REPORT IN LOCALE en-GB\n" +
				"MOVE\n" +
				"MOVE\n";

		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes(UTF_8));

		PrintStream out = new PrintStream(noopOut);

		List<Throwable> handled = new ArrayList<>();
		List<CardinalState> history = new ArrayList<>();

		Engine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 5);
		engine.register(history::add);

		CommandLineInterface cli = new CommandLineInterface(engine, in, out, (t,e) -> handled.add(e));

		cli.run();

		assertThat(history, contains(
				new CardinalState(new PlaneCoordinate(4, 3), NORTH),
				new CardinalState(new PlaneCoordinate(4, 3), EAST),
				new CardinalState(new PlaneCoordinate(4, 3), SOUTH),
				new CardinalState(new PlaneCoordinate(4, 2), SOUTH),
				new CardinalState(new PlaneCoordinate(4, 1), SOUTH)
		));
		
		assertThat(handled, contains(instanceOf(IllegalArgumentException.class)));
	}
}
