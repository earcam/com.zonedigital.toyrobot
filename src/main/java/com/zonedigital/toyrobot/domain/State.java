package com.zonedigital.toyrobot.domain;

import java.io.Serializable;
import java.util.Objects;

@SuppressWarnings("serial")
//@javax.annotation.concurrent.Immutable
public abstract class State<C extends Coordinate<C>, D extends Enum<D> & Direction<C, D>, S extends State<C, D, S>> implements Serializable {

	private final C coordinate;
	private final D direction;
	
	
	public State(C coordinate, D direction)
	{
		this.coordinate = coordinate;
		this.direction = direction;
	}

	
	public S move()
	{
		return move(1);
	}

	
	public S move(int units)
	{
		return create(direction.increment(coordinate, units), direction);
	}
	
	
	protected abstract S create(C coordinate, D direction);


	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object other)
	{
		return other instanceof State && equals((S)other);
	}
	
	
	public boolean equals(S that)
	{
		return that != null
				&& Objects.equals(that.coordinate(), this.coordinate)
				&& Objects.equals(that.direction(), this.direction);
	}
	
	
	@Override
	public int hashCode()
	{
		return Objects.hash(coordinate, direction);
	}


	public C coordinate()
	{
		return coordinate;
	}


	public D direction()
	{
		return direction;
	}
}
