package com.zonedigital.toyrobot.imp;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.spi.Command;

public class LeftCommand implements Command {

	public static final LeftCommand LEFT_COMMAND = new LeftCommand();
	
	@Override
	public CardinalState execute(CardinalState state)
	{
		return new CardinalState(state.coordinate(), state.direction().left());
	}
}
