package com.zonedigital.toyrobot.app.cli;

import static com.zonedigital.toyrobot.imp.LeftCommand.LEFT_COMMAND;
import static com.zonedigital.toyrobot.imp.MoveCommand.MOVE_COMMAND;
import static com.zonedigital.toyrobot.imp.RightCommand.RIGHT_COMMAND;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.zonedigital.toyrobot.domain.CardinalDirection;
import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.domain.PlaneCoordinate;
import com.zonedigital.toyrobot.imp.PlaceCommand;

public class CliParser {

	private static final CommandAction LEFT = new CommandAction(LEFT_COMMAND);
	private static final CommandAction RIGHT = new CommandAction(RIGHT_COMMAND);
	private static final CommandAction MOVE = new CommandAction(MOVE_COMMAND);
	private static final ReportAction REPORT = ReportAction.REPORT_ACTION;
	
	
	private static final Map<String, Function<String[], Action>> ACTIONS = new HashMap<>();
	static {
		ACTIONS.put("LEFT", s -> LEFT);
		ACTIONS.put("RIGHT", s -> RIGHT);
		ACTIONS.put("MOVE", s -> MOVE);
		ACTIONS.put("PLACE", CliParser::placeCommandAction);
		ACTIONS.put("REPORT", s -> REPORT);
	}


	private static CommandAction placeCommandAction(String[] tokens)
	{
		return new CommandAction(new PlaceCommand(parseState(tokens[1])));
	}
	

	public Action parse(String commandLine)
	{
		String trimmed = commandLine.trim();
		String[] parsed = trimmed.split("\\s+");
		if(parsed.length > 2) {
			throw new IllegalArgumentException("Invalid command: \"" + trimmed + "\"");
		}
		Function<String[], Action> action = ACTIONS.get(parsed[0]);
		if(action == null) {
			throw new IllegalArgumentException("Command not found: " + trimmed);
		}
		return action.apply(parsed);
	}


	static CardinalState parseState(String text)
	{
		String[] elements = text.split(",");
		if(elements.length != 3) {
			throw new IllegalArgumentException("Expected a CSV tuple of numbers: X,Y,F");
		}
		try {
			int x = Integer.parseInt(elements[0]);
			int y = Integer.parseInt(elements[1]);
			CardinalDirection direction = CardinalDirection.valueOf(elements[2]);
			
			return new CardinalState(new PlaneCoordinate(x, y), direction);
		} catch(NumberFormatException e) {
			throw new IllegalArgumentException("Expected a CSV tuple of numbers: X,Y,F - where X and Y are integers");
		}
	}
}
