
# Brief Notes

I've not modularised the application - this would have been worth it when using service injection (SPI, OSGi, Spring, etc),
but would have required a considerably more abstraction (to decouple CLI command parsing from the command classes).

Pluggable alternatives; the `Coordinate` and `Direction` classes could have further implementations, e.g. 3D space and intercardinal directions (SSW etc).


## QA

Test coverage is 100% for branch and line.
SonarQube only reports issue with the use of STDOUT/STDIN/STDERR in the Main class, which has been suppressed.

## Building


Was built with Maven 3.5.4 and Oracle Hotspot JDKs.

**Note:** This Maven project requires toolchains to be present a JDK >= 8, also note
that the `<vendor>` tag is not set.
Please see [the guide](https://maven.apache.org/plugins/maven-toolchains-plugin/), 
examples available 
[here](http://search.maven.org/#search|ga|1|a%3A%22io.earcam.maven.toolchain%22).


		mvn  --toolchains path/to/toolchains.xml  clean install


		# redirect from file:
		java -jar ./target/*toyrobot*.jar   <   commands_in_file.txt
		
		
		# or type on stdin (^D to exit)
		java -jar ./target/*toyrobot*.jar		