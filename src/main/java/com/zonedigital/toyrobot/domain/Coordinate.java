package com.zonedigital.toyrobot.domain;

import java.io.Serializable;

@FunctionalInterface
public interface Coordinate<C extends Coordinate<C>> extends Comparable<C>, Serializable {

	public default int dimensions()
	{
		return coordinate().length;
	}


	public abstract int[] coordinate();


	public default int compareTo(C that)
	{
		int length = dimensions();
		int diff = length - that.dimensions();
		if(diff != 0) {
			return diff;
		}
		int[] a = this.coordinate();
		int[] b = that.coordinate();
		for(int i = 0; i < length; i++) {
			diff = Integer.compare(a[i], b[i]);
			if(diff != 0) {
				return diff;
			}
		}
		return 0;
	}


	public default boolean lessThan(C that)
	{
		return compareTo(that) < 0;
	}


	public default boolean greaterThanOrEqualTo(C that)
	{
		return compareTo(that) >= 0;
	}
}
