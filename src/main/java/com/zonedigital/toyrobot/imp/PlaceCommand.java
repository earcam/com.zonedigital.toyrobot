package com.zonedigital.toyrobot.imp;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.spi.InitializingCommand;

public class PlaceCommand implements InitializingCommand {

	private CardinalState newState;

	public PlaceCommand(CardinalState state)
	{
		this.newState = state;
	}


	@Override
	public CardinalState execute(CardinalState state)
	{
		return newState;
	}

}
