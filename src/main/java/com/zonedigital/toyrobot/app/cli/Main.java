package com.zonedigital.toyrobot.app.cli;

import java.lang.Thread.UncaughtExceptionHandler;

import com.zonedigital.toyrobot.domain.PlaneCoordinate;
import com.zonedigital.toyrobot.imp.DefaultEngine;
import com.zonedigital.toyrobot.spi.Engine;

@SuppressWarnings("squid:S106")
public final class Main {
	
	private Main() {}
	
	public static void main(String[] args)
	{
		Engine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 5);
		UncaughtExceptionHandler errorHandler = (t,e) -> System.err.println(e.getMessage());
		CommandLineInterface cli = new CommandLineInterface(engine, System.in, System.out, errorHandler);
		cli.run();
	}
}
