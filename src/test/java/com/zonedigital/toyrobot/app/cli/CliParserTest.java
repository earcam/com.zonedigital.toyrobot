package com.zonedigital.toyrobot.app.cli;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class CliParserTest {

	
	
	@Nested
	public class InvalidWhen {


		@Test
		void commandLineEmpty()
		{
			try {
				new CliParser().parse("");
				fail();
			} catch(IllegalArgumentException e) {}
		}


		@Test
		void moreThanTwoArguments()
		{
			try {
				new CliParser().parse("CMD A1 A2 A3");
				fail();
			} catch(IllegalArgumentException e) {}
		}


		@Test
		void commandForNameNotFound()
		{
			try {
				new CliParser().parse("CMD_NOT_FOUND");
				fail();
			} catch(IllegalArgumentException e) {}
		}


		@Test
		void placeCommandHasIncorrectArgument()
		{
			try {
				new CliParser().parse("PLACE 1,2,3,SOUTH_WEST");
				fail();
			} catch(IllegalArgumentException e) {}
		}


		@Test
		void placeCommandHasNaNInArgument()
		{
			try {
				new CliParser().parse("PLACE 1,TWO,SOUTH");
				fail();
			} catch(IllegalArgumentException e) {}
		}
		
	}
}
