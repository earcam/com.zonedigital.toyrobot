package com.zonedigital.toyrobot.app.cli;

import java.io.InputStream;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Scanner;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.domain.PlaneCoordinate;
import com.zonedigital.toyrobot.spi.Engine;
import com.zonedigital.toyrobot.spi.StateListener;

//@javax.annotation.ParametersAreNonnullByDefault
public class CommandLineInterface implements Runnable, StateListener {
	
	private final Scanner in;
	private final PrintStream out;
	private final UncaughtExceptionHandler errorHandler;
	private final Engine engine;
	private final CliParser parser;
	private volatile CardinalState lastState;

	public CommandLineInterface(Engine engine, InputStream in, PrintStream out, UncaughtExceptionHandler errorHandler)
	{
		this.in = new Scanner(in);
		this.out = out;
		this.errorHandler = errorHandler;
		this.engine = engine;
		
		engine.register(this);
		parser = new CliParser();
	}

	@Override
	public void run()
	{
		while(in.hasNextLine()) {
			try {
				String commandLine = in.nextLine();
				Action action = parser.parse(commandLine);
				if(action instanceof ReportAction) {
					printReport();
				} else {
					engine.execute(((CommandAction)action).command());
				}
			} catch(Exception e) {
				errorHandler.uncaughtException(Thread.currentThread(), e);
			}
		}
	}

	private void printReport()
	{
		PlaneCoordinate coordinate = lastState.coordinate();
		out.print(coordinate.x());
		out.print(',');
		out.print(coordinate.y());
		out.print(',');
		out.println(lastState.direction().name());
	}

	@Override
	public void update(CardinalState state)
	{
		lastState = state;
	}
}
