package com.zonedigital.toyrobot.imp;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.domain.PlaneCoordinate;
import com.zonedigital.toyrobot.spi.Command;
import com.zonedigital.toyrobot.spi.Engine;
import com.zonedigital.toyrobot.spi.InitializingCommand;
import com.zonedigital.toyrobot.spi.StateListener;

//@javax.annotation.concurrent.ThreadSafe
public class DefaultEngine implements Engine {

	private final List<StateListener> listeners = new CopyOnWriteArrayList<>();
	
	private final PlaneCoordinate lowerBound;
	private final PlaneCoordinate upperBound;
	
	volatile CardinalState state = null;
	
	public DefaultEngine(PlaneCoordinate bottomLeft, int maxLength)
	{
		this.lowerBound = bottomLeft;
		this.upperBound = new PlaneCoordinate(bottomLeft.x() + maxLength, bottomLeft.y() + maxLength);
	}
	
	
	@Override
	public void register(StateListener listener)
	{
		listeners.add(listener);
	}


	@Override
	public synchronized void execute(Command command)
	{
		CardinalState newState = (canExecute(command)) ? command.execute(state) : null;

		update(newState);
	}


	private boolean canExecute(Command command)
	{
		return state != null || command instanceof InitializingCommand;
	}


	private void update(CardinalState newState)
	{
		if(isValid(newState)) {
			state = newState;
			updateListeners();
		}
	}


	private boolean isValid(CardinalState newState)
	{
		return newState != null && validBounds(newState.coordinate());
	}


	private boolean validBounds(PlaneCoordinate coordinate)
	{
		return coordinate.greaterThanOrEqualTo(lowerBound) && coordinate.lessThan(upperBound);
	}


	private void updateListeners()
	{
		for(StateListener listener : listeners) {
			listener.update(state);
		}
	}
}
