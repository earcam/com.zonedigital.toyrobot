package com.zonedigital.toyrobot.imp;

import static com.zonedigital.toyrobot.domain.CardinalDirection.NORTH;
import static com.zonedigital.toyrobot.domain.CardinalDirection.SOUTH;
import static com.zonedigital.toyrobot.imp.LeftCommand.LEFT_COMMAND;
import static com.zonedigital.toyrobot.imp.MoveCommand.MOVE_COMMAND;
import static com.zonedigital.toyrobot.imp.RightCommand.RIGHT_COMMAND;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.domain.PlaneCoordinate;

public class DefaultEngineTest {

	@Test
	void commandsIgnoredWhenNotPositioned()
	{
		DefaultEngine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 10);

		List<CardinalState> history = new ArrayList<>();
		engine.register(history::add);

		engine.execute(MOVE_COMMAND);
		engine.execute(LEFT_COMMAND);
		engine.execute(MOVE_COMMAND);
		engine.execute(RIGHT_COMMAND);

		assertThat(engine.state, is(nullValue()));
		assertThat(history, is(empty()));
	}

	@Nested
	public class Bounds {

		@Nested
		public class Negative {
			@Test
			public void negativeInitialPositionAllowedWhenWithinBounds()
			{
				DefaultEngine engine = new DefaultEngine(new PlaneCoordinate(-4, -4), 9);

				CardinalState state = new CardinalState(new PlaneCoordinate(-1, -3), NORTH);
				engine.execute(new PlaceCommand(state));

				assertThat(engine.state, is(equalTo(state)));
			}

		}

		@Nested
		public class OutOfBounds {

			@Test
			public void initialPositionIgnoredWhenOutOfBounds()
			{
				DefaultEngine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 5);

				CardinalState state = new CardinalState(new PlaneCoordinate(5, 5), NORTH);
				engine.execute(new PlaceCommand(state));

				assertThat(engine.state, is(nullValue()));
			}


			@Test
			public void whenMoveIsOutOfBoundsThenIgnored()
			{
				DefaultEngine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 5);

				List<CardinalState> history = new ArrayList<>();
				engine.register(history::add);

				CardinalState state = new CardinalState(new PlaneCoordinate(0, 0), SOUTH);
				engine.execute(new PlaceCommand(state));
				engine.execute(MOVE_COMMAND);

				assertThat(engine.state, is(state));
				assertThat(history, contains(state));
			}
		}
	}

	@Nested
	public class OncePositionedSubsequentCommandsExecuted {

		@Test
		public void finalState()
		{
			DefaultEngine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 10);

			engine.execute(MOVE_COMMAND);

			CardinalState s0 = new CardinalState(new PlaneCoordinate(1, 3), NORTH);
			engine.execute(new PlaceCommand(s0));
			engine.execute(LEFT_COMMAND);
			engine.execute(MOVE_COMMAND);

			CardinalState s1 = new CardinalState(s0.coordinate(), NORTH.left());
			CardinalState s2 = s1.move();

			assertThat(engine.state, is(equalTo(s2)));
		}


		@Test
		public void listeners()
		{
			DefaultEngine engine = new DefaultEngine(new PlaneCoordinate(0, 0), 10);

			List<CardinalState> history = new ArrayList<>();
			engine.register(history::add);

			engine.execute(MOVE_COMMAND);

			CardinalState s0 = new CardinalState(new PlaneCoordinate(1, 3), NORTH);
			engine.execute(new PlaceCommand(s0));
			engine.execute(MOVE_COMMAND);
			engine.execute(RIGHT_COMMAND);

			CardinalState s1 = s0.move();
			CardinalState s2 = new CardinalState(s1.coordinate(), NORTH.right());

			assertThat(history, contains(s0, s1, s2));
		}
	}
}
