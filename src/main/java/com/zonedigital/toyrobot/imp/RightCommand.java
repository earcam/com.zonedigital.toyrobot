package com.zonedigital.toyrobot.imp;

import com.zonedigital.toyrobot.domain.CardinalState;
import com.zonedigital.toyrobot.spi.Command;

public class RightCommand implements Command {

	public static final RightCommand RIGHT_COMMAND = new RightCommand();
	
	@Override
	public CardinalState execute(CardinalState state)
	{
		return new CardinalState(state.coordinate(), state.direction().right());
	}
}
