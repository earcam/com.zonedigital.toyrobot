package com.zonedigital.toyrobot.app.cli;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.is;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

public class MainTest {

	@Test
	public void asPerBrief()
	{
		String input = "PLACE 0,0,NORTH\n" +
				"MOVE\n" +
				"REPORT\n" +
				"PLACE 0,0,NORTH\n" +
				"LEFT\n" +
				"REPORT\n" +
				"PLACE 1,2,EAST\n" +
				"MOVE\n" +
				"MOVE\n" +
				"LEFT\n" +
				"MOVE\n" +
				"REPORT\n";

		String expected = "0,1,NORTH\n" +
				"0,0,WEST\n" +
				"3,3,NORTH";

		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes(UTF_8));
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(output);
		
		ByteArrayOutputStream error = new ByteArrayOutputStream();
		PrintStream err = new PrintStream(error);
		
		capture(in, out, err, () -> {
			Main.main(new String[0]);
		});

		String actual = new String(output.toByteArray(), UTF_8);
		assertThat(actual, is(equalToIgnoringWhiteSpace(expected)));
		
		assertThat(error.toByteArray().length, is(0));
	}

	
	private void capture(InputStream in, PrintStream out, PrintStream err, Runnable runnable)
	{
		InputStream oldIn = System.in;
		PrintStream oldOut = System.out;
		PrintStream oldErr = System.err;
		try {
			System.setIn(in);
			System.setOut(out);
			System.setErr(err);

			runnable.run();
			
		} finally {
			System.setIn(oldIn);
			System.setOut(oldOut);
			System.setErr(oldErr);
		}
		
		
	}


	@Test
	public void failedCommandIsHandled()
	{
		String input = "PLACE 4,3,2,1,SSW\n";

		ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes(UTF_8));
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(output);
		
		ByteArrayOutputStream error = new ByteArrayOutputStream();
		PrintStream err = new PrintStream(error);
		
		capture(in, out, err, () -> {
			Main.main(new String[0]);
		});

		String actualOut = new String(output.toByteArray(), UTF_8);
		assertThat(actualOut, is(emptyString()));
		
		String actualErr = new String(error.toByteArray(), UTF_8);
		assertThat(actualErr, containsString("Expected"));
	}

}
