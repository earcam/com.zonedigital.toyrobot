package com.zonedigital.toyrobot.domain;

import static com.zonedigital.toyrobot.domain.CardinalDirection.NORTH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


public class CardinalStateTest {


	
	@Nested
	public class Transition {
	
		@Test
		void moveDelegatesToDirection()
		{
			PlaneCoordinate coordinate = new PlaneCoordinate(0, 0);
			CardinalDirection direction = NORTH;

			CardinalState moved = new CardinalState(coordinate, direction).move();
			
			assertThat(moved.coordinate(), is(equalTo(direction.increment(coordinate))));
		}
	}

}
